<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController');

Route::get('/redirects', 'RedirectController@index')->name('redirects');
Route::post('/redirects', 'RedirectController@store');
Route::put('/redirects/{redirect}', 'RedirectController@update');
Route::delete('/redirects/{redirect}', 'RedirectController@destroy');

Route::get('/redirects/{redirect}/visits', 'RedirectController@show')->name('redirect.visits');

Route::get('/link/{redirect:identifier}', 'PerformRedirectController');

Route::get('/visits', 'VisitController@index');
