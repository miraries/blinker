@extends('layout')

@section('title', 'Redirect Management')

@section('style')
    <style>
        @media only screen and (max-width: 800px) {

            .breakable-table table,
            .breakable-table thead,
            .breakable-table tbody,
            .breakable-table th,
            .breakable-table td,
            .breakable-table tr {
                display: block;
            }

            .breakable-table thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            .breakable-table tr {
                border: 1px solid #ccc;
            }

            .breakable-table td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                padding-left: 50%;
                white-space: normal;
                text-align: left;
            }

            .breakable-table td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
                text-align: left;
                font-weight: bold;
            }

            .breakable-table td:before {
                content: attr(data-title);
            }

            .breakable-table tr th {
                display: none;
            }
        }

        .tippy-box[data-theme~="gradient"] {
            box-shadow: rgb(201, 160, 255) 0px 8px 12px;
            font-weight: bold;
            background: linear-gradient(130deg, rgb(80, 123, 244), rgb(255, 139, 203));
        }
    </style>
@endsection

@section('content')
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit redirect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="dynamic" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="modal-identifier" class="col-form-label">Identifier</label>
                            <input type="text" class="form-control" id="modal-identifier" name="identifier" disabled>
                        </div>
                        <div class="form-group">
                            <label for="modal-url" class="col-form-label">Url*</label>
                            <input type="text" class="form-control" id="modal-url" name="url" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Add redirect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/redirects" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="modal-identifier" class="col-form-label">Identifier</label>
                            <input type="text" class="form-control" id="modal-identifier" name="identifier"
                                   placeholder="automatically generate">
                        </div>
                        <div class="form-group">
                            <label for="modal-url" class="col-form-label">Url*</label>
                            <input type="text" class="form-control" id="modal-url" name="url" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Delete redirect</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="dynamic" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <span>Are you sure you want to delete this redirect?</span>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <h1 class="display-4 mb-4">Redirects</h1>
            </div>
            <div class="col align-text-bottom text-right align-self-center">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">Add new
                </button>
            </div>
        </div>

        <div>
            @error('url', 'identifier')
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @enderror
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ Session::get('success') }}
            </div>
        @endif

        <table class="table table-striped breakable-table">
            <thead class="thead-light">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Identifier</th>
                <th scope="col">URL</th>
                <th scope="col">Visits</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($redirects as $redirect)
                <tr>
                    <th scope="row" data-title="ID">{{ $redirect->id }}</th>
                    <td data-title="Identifier">
                        <span role="button" data-tippy-content="Click to copy" class="copyable clickable">
                            {{ $redirect->identifier }}
                        </span>
                    </td>
                    <td data-title="URL">
                        <a href="{{ $redirect->normalizedUrl }}" rel="noreferrer"
                           target="_blank">{{ $redirect->normalizedUrl }}</a>
                    </td>
                    <td data-title="Visits">
                        <a href="{{ route('redirect.visits', $redirect->id) }}">{{ $redirect->visits_count }}</a>
                    </td>
                    <td data-title="Actions">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#editModal"
                                data-identifier="{{ $redirect->identifier }}" data-url="{{ $redirect->url }}"
                                data-id="{{ $redirect->id }}">Edit
                        </button>
                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#deleteModal" data-id="{{ $redirect->id }}">Delete
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('script')
    <script>
        $('#editModal').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget)
            const identifier = button.data('identifier')
            const url = button.data('url')
            const id = button.data('id')
            const modal = $(this)

            modal.find('.modal-body #modal-identifier').val(identifier)
            modal.find('.modal-body #modal-url').val(url)

            modal.find('form').attr('action', '/redirects/' + id)
        })

        $('#deleteModal').on('show.bs.modal', function (event) {
            const button = $(event.relatedTarget)
            const id = button.data('id')

            $(this).find('form').attr('action', '/redirects/' + id)
        })

        $('.copyable').on('click', function (event) {
            navigator.clipboard.writeText(`${window.location.origin}/link/${event.target.textContent.trim()}`)
        })

        tippy('[data-tippy-content]', {
            hideOnClick: false,
            trigger: 'mouseenter',
            animation: 'scale'
        })

        $('[data-tippy-content]').click(function (event) {
            this._tippy.setProps({
                content: 'Copied!',
                theme: 'gradient',
                arrow: false,
            });

            setTimeout(() => {
                this._tippy.setProps({
                    content: 'Click to copy',
                    theme: '',
                    arrow: true,
                });
            }, 3000)
        })

        const getCurrentVisits = () => {
            return Object.fromEntries($('tbody tr').map(function () {
                const elem = $(this).find('td[data-title=Visits]')
                const id = $(this).find('th[data-title=ID]').text().trim()

                return [[id, {
                    elem,
                    visits: elem.text().trim(),
                }]]
            }).toArray())
        }

        const getOldVisits = () => JSON.parse(localStorage.getItem('redirects_old_visits'));
        const setOldVisits = (visits) => {
            Object.entries(visits).forEach(([key, val]) => delete visits[key].elem);
            localStorage.setItem('redirects_old_visits', JSON.stringify(visits));
        }

        $(function () {
            const oldVisits = getOldVisits()
            const currentVisits = getCurrentVisits()

            if (!oldVisits) {
                setOldVisits(currentVisits)
                return
            }

            for (let [id, redirect] of Object.entries(currentVisits)) {
                if(oldVisits.hasOwnProperty(id) && oldVisits[id].visits !== redirect.visits)
                    redirect.elem.append('<span class="badge badge-secondary">New</span>')
            }

            setOldVisits(currentVisits)
        })
    </script>
@endsection
