@extends('layout')

@section('title', 'Visit Logs')

@section('style')
    <style>
        html {
            overflow-y: scroll;
        }

        .timestamp span:nth-child(2) {
            display: none;
        }

        .timestamp:hover span:first-child {
            display: none;
        }

        .timestamp:hover span:nth-child(2) {
            display: inline-block;
        }

        .collapser:hover {
            color: var(--blue);
        }

        .list-group-item {
            transition: background-color 2s;
        }

        .notransition {
            transition: none !important;
        }

        .bg-mildwarning {
            background-color: #ffe1bc;
        }
    </style>
@endsection

@section('content')

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col">
                <h1 class="display-4 mb-4">Visits</h1>
            </div>
        </div>

        <div class="list-group">
            @foreach($visits as $visit)
                <div class="list-group-item notransition">
                    <div class="d-flex w-100 justify-content-between">
                        <h4 class="mb-1">
                            {{ $visit->redirect->identifier }}
                            <small class="text-muted text-truncate">
                                {{ $visit->redirect->normalizedUrl }}
                                <span class="badge badge-warning d-none">New</span>
                            </small>
                        </h4>
                        <small class="timestamp">
                            <span>{{ $visit->visited_at->diffForHumans() }}</span>
                            <span>{{ $visit->visited_at }}</span>
                            <x-icon class="ml-4 clickable collapser" name="arrows-angle-expand"></x-icon>
                        </small>
                    </div>

                    <p class="mb-1">{{ $visit->short_data }}</p>
                    <small class="mb-1 d-block text-truncate">
                        <strong>UserAgent </strong>
                        {{ $visit->user_agent }}
                    </small>
                    <small class="mb-1 d-block text-truncate">
                        <strong>BlinkerID </strong>
                        @if(isset($visit->data['new_id']))
                            <span class="badge badge-primary">New</span>
                        @endif
                        {{ $visit->blinker_id }}
                    </small>
                    <div class="collapse mt-3">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggles="headers" data-toggle="tab" href="#" role="tab">Headers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggles="cookies" data-toggle="tab" href="#"
                                   role="tab">Cookies</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggles="ipinfo" data-toggle="tab" href="#" role="tab">IP
                                    Info</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" data-toggled="headers" role="tabpanel">
                                <table class="table table-sm mt-3">
                                    <thead>
                                    <tr>
                                        <th scope="col">Header</th>
                                        <th scope="col">Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($visit['data']['headers'] as $key => $value)
                                        <tr>
                                            <th scope="row">{{ $key }}</th>
                                            <td class="text-break">{{ $value }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" data-toggled="cookies" role="tabpanel">
                                <table class="table table-sm mt-3">
                                    <thead>
                                    <tr>
                                        <th scope="col">Cookie</th>
                                        <th scope="col">Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($visit['data']['cookies'] as $key => $value)
                                        <tr>
                                            <th scope="row">{{ $key }}</th>
                                            <td class="text-break">{{ $value }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" data-toggled="ipinfo" role="tabpanel">
                                <table class="table table-sm mt-3">
                                    <thead>
                                    <tr>
                                        <th scope="col">Key</th>
                                        <th scope="col">Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($visit['data']['geoip'] as $key => $value)
                                        <tr>
                                            <th scope="row">{{ $key }}</th>
                                            <td>{{ $value }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.collapser').click(function () {
            const collapsable = $(this).parents('.list-group-item').find('.collapse').collapse('toggle')
        });

        // $(document).on('show.bs.collapse', function (event) {
        //     setTimeout(() => $(event.target).parents('.list-group-item').get(0).scrollIntoView({ block: 'center', behavior: 'smooth' }), 200)
        // });

        $('[data-toggles]').click(function () {
            const toggles = $(this).attr('data-toggles')
            const parent = $(this).parents('.collapse')
            parent.find('[data-toggled]').removeClass(['active', 'show'])
            parent.find(`[data-toggled='${toggles}']`).addClass(['active', 'show'])
        })

        const currentVisits = @json($visits->pluck('id'));
        const getOldVisits = () => JSON.parse(localStorage.getItem('visits_old_visits'));
        const setOldVisits = (visits) => localStorage.setItem('visits_old_visits', JSON.stringify(visits))

        $(function () {
            const oldVisits = getOldVisits()

            if (!oldVisits) {
                setOldVisits(currentVisits)
                return
            }

            currentVisits.map((val, key) => {
                if(!oldVisits.includes(val)){
                    const elem = $('.list-group-item').eq(key)
                    elem.find('.badge').removeClass('d-none')
                    elem.addClass('bg-mildwarning')
                    setTimeout(() => elem.removeClass(['notransition', 'bg-mildwarning']), 3000)
                }
            })

            setOldVisits(currentVisits)
        })
    </script>
@endsection
