<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Icon extends Component
{
    private $name;

    /**
     * Create a new component instance.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.icon', ['icon' => $this->name]);
    }
}
