<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Visit
 * @package App
 */
class Visit extends Model
{
    public $timestamps = false;

    protected $casts = [
        'data' => 'array'
    ];

    protected $dates = [
        'visited_at',
    ];

    public function redirect()
    {
        return $this->belongsTo(Redirect::class);
    }

    public function getShortDataAttribute()
    {
        $data = collect($this->data['geoip']);
        $columns = ['city', 'country', 'ip'];

        return $data->only($columns)->implode(', ');
    }

    public function getUserAgentAttribute()
    {
        return $this->data['headers']['HTTP_USER_AGENT'];
    }

    public function getBlinkerIdAttribute()
    {
        return $this->data['cookies']['blinker_id'] ?? 'none';
    }
}
