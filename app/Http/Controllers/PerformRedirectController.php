<?php

namespace App\Http\Controllers;

use App\Redirect;
use App\Visit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class PerformRedirectController extends Controller
{
    private function serverData()
    {
        $keys = [
            'REMOTE_ADDR',
            'HTTP_USER_AGENT',
            'HTTP_REFERER',
            'QUERY_STRING',
            'REMOTE_HOST',
            'REMOTE_PORT',
            'SERVER_PROTOCOL',
            'HTTP_ACCEPT',
            'HTTP_X_REAL_IP',
            'HTTP_X_FORWARDED_HOST',
            'HTTP_X_FORWARDED_SERVER'
        ];

        return collect($keys)->mapWithKeys(function ($item) {
            return [$item => $_SERVER[$item] ?? null];
        })->toArray();
    }

    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Redirect $redirect
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Redirect $redirect)
    {
        $newId = null;
        $cookies = $_COOKIE;

        if (empty(Cookie::get('blinker_id'))) {
            $newId = Str::random(16);
            $cookies = array_merge($cookies, ['blinker_id' => $newId]);

            Cookie::queue('blinker_id', $newId, 2147483647);
        }

        $visit = new Visit();
        $visit->visited_at = now();
        $visit->redirect_id = $redirect->id;
        $visit->data = array_merge(
            ['headers' => $this->serverData()],
            ['geoip' => geoip($request->ip())->toArray()],
            ['cookies' => $cookies],
            $newId ? ['new_id' => true] : []
        );

        $visit->save();

        return redirect()->away($redirect->normalizedUrl);
    }
}
