<?php

namespace App\Http\Controllers;

use App\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RedirectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('redirects', [
            'redirects' => Redirect::query()->withCount('visits')->get()
        ]);
    }

    public function show(Redirect $redirect)
    {
        return view('redirect_with_visits', [
            'redirect' => $redirect->load('visits')->loadCount('visits')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'identifier' => 'nullable|string|unique:redirects',
            'url' => 'required|string'
        ]);

        $identifier = $request->filled('identifier') ? $request->identifier : Str::random(16);
        $url = $request->url;

        Redirect::create(compact('identifier', 'url'));

        return \redirect('redirects')->with('success', 'Added successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Redirect  $redirect
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Redirect $redirect)
    {
        $request->validate([
            'url' => 'required|string'
        ]);

        $redirect->update(['url' => $request->url]);

        return \redirect('redirects')->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Redirect $redirect
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Redirect $redirect)
    {
        $redirect->delete();

        return \redirect('redirects')->with('success', 'Deleted successfully');
    }
}
