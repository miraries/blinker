<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $fillable = [
        'identifier', 'url',
    ];

    public function visits()
    {
        return $this->hasMany(Visit::class)->latest('visited_at');
    }

    public function getNormalizedUrlAttribute()
    {
        if ($ret = parse_url($this->url)) {
            if (!isset($ret["scheme"])) {
                return "https://{$this->url}";
            }
        }

        return $this->url;
    }
}
